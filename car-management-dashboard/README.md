## The Project

- Run Project

yarn dev
npm dev

- DB Diagram
  <br>
  https://dbdiagram.io/d/626169521072ae0b6ac5e9a2
  <img src="ERD/erd.png" alt="Page" width="250" height="300">

- Page Dashboard
  <br>
  <img src="public/images/capture/dashboard.png">

- Page Cars
  <br>
  <img src="public/images/capture/cars.png">

- Page Filter Cars Size
  <br>
  <img src="public/images/capture/small.png">
  <img src="public/images/capture/medium.png">
  <img src="public/images/capture/large.png">

- Page Add Car
  <br>
  <img src="public/images/capture/add.png">

- Page Edit Car
  <br>
  <img src="public/images/capture/edit.png">


### Link Repository

https://gitlab.com/LadiaRahmahati/car-management-dashboard



## Info

- Get all cars : GET <code>/cars</code>
- Create a car : POST <code>/cars/create</code>
- Update a car : PUT <code>/cars/update/:id</code>
- Delete a car : DELETE <code>/cars/delete/:id</code>

- Filter size car : GET <code>/cars/search/?car_size=</code>
- Search cars by name and size : GET <code>/cars/search/?q=</code>
